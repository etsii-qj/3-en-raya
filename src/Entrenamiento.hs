{-# LANGUAGE DeriveGeneric #-}
module Entrenamiento(
  cargaTabla,
  accionOptima,
  Jugador(Ninguno,Uno,Dos),
  State,
  QTable,
  Fin(Lleno,Ganador,Continua),
  esFin,
  accion,
  iteracion
  )where

import Control.Exception (catch, SomeException)
import System.Environment (getArgs)
import qualified Data.Vector as V
import qualified Data.Map as Map
import System.Random
import Data.List.Split
import System.Directory

--Declaraciones de tipos:
type Entorno = (State,Jugador,Fin)
type State = V.Vector Char
type Recompensa = Double
type Accion = Int -- del 1 - 9
type QTable = Map.Map String (V.Vector Recompensa)

data Fin = Lleno | Ganador | Continua
         deriving (Show, Eq)
data Jugador = Ninguno | Uno | Dos
             deriving (Show,Eq)

--Nombre del fichero en el que se va a trabajar
fichero :: String
fichero = "/tmp/temporal"

-- El entorno comienza sin ninguna casilla rellena "---------
entorno :: Entorno
entorno =(V.generate 9 (\i -> '-'), Ninguno, Continua)


cargaTabla :: Integer -> IO QTable
cargaTabla iter = do
  let fichero_enumerado = fichero ++ show (iter-1) ++ ".txt"
  existe <- doesFileExist $ fichero_enumerado
  let res = Map.fromList inicializa
  out <- if existe
    then do
    res <- leeTabla fichero_enumerado
    return res
    else
    return res
  return out


leeTabla :: String -> IO (Map.Map [Char] (V.Vector Recompensa))
leeTabla f = do
  cad <- readFile f
  let lineas = lines cad
  let tabla = map (\l -> splitOn " " l) lineas
  let qtabla = map (\(c) -> (c !! 0, V.fromList $ map (\e-> read e :: Double) $
                             splitOn "," $
                             substring 1 (length (c !! 1) - 1) (c !! 1)))  tabla
  let res = Map.fromList qtabla
  return res
        where substring start end text = take (end - start) (drop start text)



inicializa :: [(String,(V.Vector Recompensa))]
inicializa = aux 1 els
  where
    els = ["-","x","o"]
    aux 9 xs =  validos xs
    aux n xs = aux (n+1) (siguientes xs)
    siguientes xs = [x ++ e | x<-xs, e<-els]
    validos xs = [(x,V.generate 9 (\i -> if x!!i == '-' then 0 else -5)) | x<-xs, (valido x)]
    valido a = ((diff a) == 1 || (diff a) == 0 ) -- reducimos a casos que puedan
      -- tener lugar, teniendo en cuenta que la máquina solo se considera a sí
      -- misma como 'o' por tanto siempre habrán tanto el mismo numero de 'o' y 'x' o habrá solo
      -- un 'x' más que 'o'.
    diff a = (ocurrencias 'x' a) - (ocurrencias 'o' a)
    ocurrencias c str = length(filter (==c) str)



accion :: Entorno -> Accion -> QTable-> (Entorno, Recompensa)
accion (s, j, f) a qt | f' == Ganador = ((s,j,f'),15)
                      | f' == Lleno = ((s,j',f'),0)
                      | otherwise = ((s',j',f'), reward)
  where s' = s V.// [((a),mapea j)]
        mapea j = if (s V.! (a) == '-')
                  then
                    (if j == Dos then 'x' else 'o')
                  else error ("Step incorrecto" ++ show s)
        j'= if j == Dos
            then Uno
            else Dos
        f' = esFin (s')
        reward = computaRecompensa (s,j) a qt

esFin :: State -> Fin
esFin s = if (lleno s) then Lleno else (if (ganador s) then Ganador else Continua)
  where lleno e = (V.length (V.filter (\el -> el == '-') e) == 0)
        ganador e = (ganador1 e || ganador2 e || ganador3 e)
        ganador1 e = V.or $ V.map (\el-> iguales el) $ V.generate 3 (\i ->
                                                                      (V.generate 3 (\j ->
                                                                                      e V.! (3*i+j))))
        ganador2 e = V.or $ V.map (\el-> iguales el) $ V.generate 3 (\i ->
                                                                      (V.generate 3 (\j ->
                                                                                      e V.! (i+3*j))))
        ganador3 e = V.or $ V.map (\el-> iguales el) $ V.generate 2 (\i ->
                                                                      (V.generate 3 (\j ->
                                                                                      e V.! (3*j+j+(i*(2-j-j))))))
        iguales (v) = (v V.! 0 == v V.! 1) && (v V.! 1 == v V.! 2) && v V.! 0 /= '-'


computaRecompensa :: (State,Jugador) -> Accion -> QTable -> Recompensa
computaRecompensa (s,Dos) a qt= (qt Map.! ns) V.! (a)
  where ns = [if l == 'x' then 'o' else (if l == 'o' then 'x' else '-') | l <- V.toList s] -- La máquina siempre es o, luego tendremos en cuenta eso para la tabla
computaRecompensa (s,_) a qt = (qt Map.! V.toList s) V.! (a)

-- Algoritmo de entrenamiento:
alpha::Double
alpha = 0.1

gamma::Double
gamma = 0.6

epsilon::Double
epsilon = 1

--Algoritmo principal al que llamamos desde el main
iteracion :: (Fractional a, Ord a) => QTable -> a -> Integer -> IO ()
iteracion qv iters n_iteracion= entrena 0 qv iters n_iteracion

 --Bucle principal

entrena :: (Ord a, Fractional a) => a -> QTable -> a -> Integer -> IO ()
entrena n qv iters iter =
  
  if (n < iters) then do
    let epsilon = epsilon * (1-  (n/(iters*10)))
    let env = entorno
    let done = Continua
    entrena_aux n env done qv iters iter
  else do
    escribe iter $ Map.toList $ qv
    print "Fin"

escribe :: Integer -> [(String, (V.Vector Double))] -> IO()
escribe iter xs = do
  let fichero_numerado = fichero ++ (show iter) ++ ".txt"
  print(fichero_numerado)
  writeFile fichero_numerado ""
  let cad = concatena xs
  writeFile fichero_numerado cad

concatena :: Show a => [([Char], a)] -> [Char]
concatena ([(k,v)])  = k ++ " " ++ show v
concatena ((k,v):vs) = k ++ " " ++ show v ++ "\n" ++ concatena vs 

 --Algoritmo principal
entrena_aux:: (Ord t, Fractional t) =>t -> Entorno -> Fin-> QTable -> t-> Integer -> IO ()
entrena_aux n env Ganador qv iters iter = do
  entrena (n+1) qv iters iter
entrena_aux n env Lleno qv iters iter = entrena (n+1) qv iters iter
entrena_aux n env _ qv iters iter= do
  a <- eligeAccion env qv
  let (s_old, j_old, _) =  env
  let (env',r) = accion env a qv
  let (s,j,f) = env'
  let old_value = computaRecompensa (s_old, j_old) a qv
  a' <- eligeAccion env' qv
  let (envs,next_max) = accion env' (fromIntegral $ a') qv
  let (sn,jn,fn) = envs
  as <- eligeAccion envs qv
  let (_,next_next_max) = accion envs (fromIntegral $ as) qv
  let new_value = (1 - alpha) * old_value + alpha * (r - next_max + gamma * next_next_max)
  let nueva_array = sustituye (qv Map.!((siguienteEstado j_old s_old))) a new_value
  let qv2 = actualizaMap (siguienteEstado j_old s_old) (nueva_array) qv
  entrena_aux n env' f qv2 iters iter

sustituye :: (V.Vector Recompensa) -> Accion -> Double -> (V.Vector Recompensa)
sustituye v a new_value =  v V.// [(a,new_value)]

actualizaMap ::  Ord k => k -> a -> Map.Map k a -> Map.Map k a
actualizaMap key value map = Map.insert key value map


eligeAccion :: Num b => (State, Jugador, t) -> QTable -> IO b
eligeAccion env qv = do
    r <- randomRIO (0::Double,1)
    let (s,j,f) = env
    a <- (accionAleatoria s)
    let b = (accionOptima j s qv)
    let pr = if (r < epsilon) then a else fromIntegral b
    return $ fromIntegral pr

--Compara las acciones para ver con cual obtiene una mejor recompensa
accionOptima :: Jugador -> State -> QTable -> Int
accionOptima j s qv = fromIntegral $ V.maxIndex $ v
  where v = qv Map.! (siguienteEstado j s)

--Si el jugador actual es el jugador dos cambia el estado para que las x sean
        --las o y viceversa       
siguienteEstado :: Jugador -> State -> [Char]
siguienteEstado j s | j == Dos = [if l == 'x' then 'o' else (if l == 'o' then 'x' else '-') | l <- V.toList s]
       | otherwise = V.toList s

--Dado un estado, escogemos una acción aleatoria de entre las posibles
accionAleatoria :: State -> IO Integer
accionAleatoria s = do
  ret <- (randomElement $ accionesPosibles s) >>= (\x -> return $ fromIntegral x)
  return ret

--Dado un entorno, comprobamos las posibles acciones a realizar
accionesPosibles :: State -> [Int]
accionesPosibles environment = V.toList $ V.filter (\e -> e /= (-1)) $ V.imap (\i a -> if (a == '-') then i else (-1)) environment 

--Dado una lista de elementos, cogemos uno aleatorio
randomElement :: [a] -> IO a
randomElement l = do
    i <- randomRIO (0, length l - 1)
    return $ l !! i





