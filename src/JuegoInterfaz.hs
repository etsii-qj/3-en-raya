{-# LANGUAGE OverloadedStrings #-} -- pragma necesario
module JuegoInterfaz(
  inicio,
  manejaEvento,
  taab,
  hayGanador,
  tableroVacio,
  tableroLleno,
  jugadaPosible,
  Estado,
  Tablero,
  JugadorI
)where

import CodeWorld
import qualified Data.Vector as V
import Data.Matrix as M
import qualified Entrenamiento as E
import Entrenamiento (Jugador  (Ninguno, Uno, Dos))
import Entrenamiento (Fin (Ganador, Lleno, Continua))

type Estado = (Int, Int, Tablero, JugadorI) -- (posXCursor, posYCursor, tableroActual, jugadorActual)
type Tablero = Matrix Int -- Matriz del tablero que contiene el valor para
               -- las casillas del tablero
type JugadorI = (Int,Bool) -- (0 -> posVacía, 1 -> jugador1, 2 -> jugador2)
type Ficha = Double -> Double -> Picture

inicio :: E.QTable -> [Bool] -> (Estado, E.QTable, [Bool])
inicio qt ia= ((0, 0, tableroVacio, (1,compruebaIA ia 1)), qt, ia)

tableroVacio :: Tablero
tableroVacio =  matrix 3 3 (\(i,j)-> 0)





-- Devuelve el picture para el estado actual
taab ::  (Estado, E.QTable, [Bool]) -> Picture
taab ((x,y, tab,j),qt, ia)
  | jug < 0 = victoria (-jug) & pintaEstado(x, y, tab, j ) & muestraFichas tab & tablerod t & fondo t 
  |otherwise = pintaEstado(x, y, tab, j ) & muestraFichas tab & tablerod t & fondo t 
  where t = 3
        jug = fst j



-- Dado un evento cambia el estado (solo si es un estado posible)
manejaEvento :: Event -> (Estado,E.QTable, [Bool]) -> (Estado,E.QTable, [Bool])
manejaEvento _ ((_,_,t,(j,True)),qt,ia) | f' == Ganador = ((0,0,t',((-j),False)),qt,ia)
                                        | f' == Lleno = ((0,0,tableroVacio, siguienteJugador ia (j,True)),qt,ia)
                                        | otherwise = ((0,0,t', siguienteJugador ia (j,True)),qt,ia)
  where (t',f') = (juegaIA(t,j) qt)

manejaEvento (KeyPress "Right") ((x, y, t, j),qt,ia) | x < (1) && (fst j) >= 0 =((x+1, y ,t, j),qt,ia)
                                             | otherwise = ((x, y, t, j),qt,ia)

manejaEvento (KeyPress "Left") ((x, y,t,j),qt,ia)  | x > (-1) && (fst j) >= 0 = ((x-1, y,t,j),qt,ia)
                                           | otherwise = ((x, y, t, j),qt,ia)

manejaEvento (KeyPress "Up") ((x, y,t,j),qt,ia)    | y < 1 && (fst j) >= 0= ((x, y+1,t,j),qt,ia)
                                           | otherwise = ((x, y, t, j),qt,ia)

manejaEvento (KeyPress "Down") ((x, y,t,j),qt,ia)  | y > -1 && (fst j) >= 0= ((x, y-1,t,j),qt,ia)
                                           | otherwise = ((x, y, t, j),qt,ia)
manejaEvento (KeyPress "Enter") ((x,y,t,j),qt,ia)
  | (fst j)<0 = inicio qt ia
  | hayGanador (x+2,y+2,t,j) && jugadaPosible (x+2,y+2,t)=  ((0,0,actualizaTablero(x+2,y+2,t,j),((-(fst j)),False)),qt,ia)
  | tableroLleno (x+2,y+2,t, j) =  ((0,0,tableroVacio, siguienteJugador ia j),qt,ia)
  | jugadaPosible (x+2,y+2,t) = ((0, 0, actualizaTablero(x+2,y+2,t,j), siguienteJugador ia j),qt,ia)
  | otherwise = ((x,y,t,j),qt,ia)
manejaEvento _ ((x,y,t,j),qt,ia) = ((x, y, t, j),qt,ia)

compruebaIA :: [a] -> Int -> a
compruebaIA ia i = ia!!(i-1)

juegaIA :: (Tablero, Int) -> E.QTable -> (Tablero, Fin)
juegaIA (t,j) q' = (actualizaTablero (1 + fromIntegral (div a' 3),1 + fromIntegral (mod a' 3), t, (j,True)),fn)
  where j' | j == 2 = Dos
           | otherwise = Uno
        s' = V.fromList $ map (\e-> if (e==0) then '-' else (if (e == 1) then 'o' else 'x')) m'
        m' = toList t
        ((sn,jn,fn),_) = E.accion (s',j',Continua) a' q'
        a' = E.accionOptima j' s' q'


-- Booleanos para comprobar si hay ganador, el tablero esta lleno o jugada es
  -- posible en funcion al estado al que se quería pasar a través de manejaEvento
hayGanador :: (Int, Int, Tablero, JugadorI) -> Bool
hayGanador (x,y,t,j) = vertical nt x y j || horizontal nt x y j || diagonal1 nt x y j || diagonal2 nt x y j
  where nt = actualizaTablero(x,y,t,j)
        vertical aux x y j =  iguales [aux!(x,1) , aux!(x,2)  , aux!(x,3) , fst j]
        horizontal aux x y j  = iguales[aux!(1,y) , aux !(2,y) , aux !(3,y) , fst j]
        diagonal1 aux x y j = iguales[aux !(1,1) , aux !(2,2) , aux !(3,3) , fst j]
        diagonal2 aux x y j = iguales[aux !(1,3) , aux !(2,2) , aux !(3,1) , fst j]

--Funcion para comprobar si las fichas de las casillas coinciden, pudiendo así
        --comprobar si alguien ha ganado
iguales :: (Eq a, Integral a, Ord a) => [a] -> Bool
iguales [x] = True
iguales (x:y:xs) = x == y && iguales (y:xs)


tableroLleno :: Estado -> Bool  
tableroLleno (x,y,t,jug) = and [nt!(i,j)  /= 0 | i <- [1,2,3] , j <- [1,2,3]]
  where nt = actualizaTablero (x,y,t,jug)

jugadaPosible :: (Int, Int, Tablero) -> Bool
jugadaPosible (x, y, t) = t!(x,y) == 0

-- A partir de un estado, devuelve un tablero
actualizaTablero :: Estado -> Tablero
actualizaTablero (x,y,t,jug) = setElem (fst jug) (x,y) t

-- Alterna entre jugadores
siguienteJugador :: [Bool] -> JugadorI -> JugadorI
siguienteJugador ia j | fst j == 1 = (2, compruebaIA ia 2)
                      | fst j == 2 = (1, compruebaIA ia 1)
                      |otherwise = error "Jugador no valido"

-- Mensaje de victoria al finalizar la partida con el color del jugador vencedor
victoria :: Int -> Picture
victoria j = colored (if (j) == 1 then red else green) (text "Tenemos ganador") & colored white (solidRectangle 12 2.2)

-- Estado del cursor con color según jugador actual
pintaEstado :: Estado -> Picture
pintaEstado (x, y, t, j) = translated (3*(fromIntegral x)) (3*(fromIntegral y)) (colored (if (fst j) == 1 then red else green) (rectangle 2.5 2.5))

-- Dado un tablero, muestra las fichas según el jugador al que pertenezcan
muestraFichas :: Tablero -> Picture
muestraFichas t = pictures [ pictures [ (if t!(row,col) /= 0 then (ficha (t!(row,col)) (fromIntegral (row-2)) (fromIntegral (col-2))) else blank) | col <- [1..3]] | row <- [1..3] ]
  where ficha pos ix iy | pos == 1 = circulo ix iy
                        | pos == 2 = cruz ix iy
                        | otherwise = error "error mostrando ficha"

-- Ficha "O" para jugador1
circulo :: Ficha
circulo i j  = translated (i*3) (j*3) (colored red (thickCircle (0.2) (1.2)))

-- Ficha "X" para el jugador 2
cruz :: Ficha
cruz i j = translated (i*3) (j*3) (colored green ((rotated (pi/4) linea ) & (rotated (-pi/4) linea )))
  where linea = (thickPolyline (0.2) [(-1.5,0), (1.5,0)])

-- Picture del tablero desplazado para que se encuentre centrado en la pantalla
tablerod:: Double -> Picture
tablerod n = translated a a (pictures [ translated (-3*a) 0 (lineasv n) | a<- [1..n-1]] &
 pictures [ translated 0 (-3*a)  (lineash n) | a<- [1..n-1]])
  where a = (n*2)-n/2

lineasv :: Double -> Picture
lineasv n = pictures [ translated 0 (-3*a) lineav | a<- [1..n]]
  where lineav = thickPolyline 0.075 [(0,-0.5), (0,3.5)]

lineash :: Double -> Picture
lineash n = pictures [ translated (-3*a) 0 lineah | a<- [1..n]]
  where lineah = thickPolyline 0.075 [(-0.5,0),(3.5,0)]

fondo :: Double -> Picture        
fondo n = colored azure (solidRectangle (300*n) (300*n)) 

