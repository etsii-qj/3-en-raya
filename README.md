# 3 en Raya
Se pretende mediante el uso de diferentes algoritmos de aprendizaje automático, la elaboración de un programa que sea capaz de jugar al tres en raya en el lenguaje Haskell.
Se han tenido en cuenta varios algoritmos, a partir de estos, se obtendrá la mejor implementación y por tanto solución posible para el problema. Por otro lado, mediante la librería de Haskell ''CodeWorld'' se implementará una interfaz sencilla para jugar al juego.

* Esqueleto (3 en raya)
  * Diseños para el juego (Tablero y Fichas).
  * Implementación del juego (Reglas, movimientos, fin de juego, etc.).
  * Algoritmo de juego (Todo lo que hace posible que una IA decida una acción sobre el tablero durante su turno).
